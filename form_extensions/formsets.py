from __future__ import absolute_import, unicode_literals

from django.core.exceptions import ValidationError
from django.forms.formsets import ManagementForm
from django.forms.models import BaseInlineFormSet


class BetterInlineFormSet(BaseInlineFormSet):
    """
    Allows saving formsets without validation if requested, by passing
    ignore_errors=True to save().

    Also allows setting extra in the constructor, avoiding the need to redefine
    the formset class just to change it. This was fixed post django 1.5.
    """

    def __init__(self, *args, **kwargs):
        if 'extra' in kwargs:
            self.extra = kwargs.pop('extra')
        super(BetterInlineFormSet, self).__init__(*args, **kwargs)

    def save(self, commit=True, ignore_errors=False):
        """Saves model instances for every form, adding and changing instances
        as necessary, and returns the list of instances.
        """
        self.ignore_errors = ignore_errors

        for form in self:
            # If the parent instance was previously unsaved, then the subforms
            # were constructed without FK values by
            # BaseInlineFormSet._construct_form, so we need to update it now
            # so that they can be saved without violating DB constraints.

            if getattr(form.instance, self.fk.get_attname()) is None:
                # We don't need to set the raw FK field, because assigning
                # the instance to the related field below will do the same.
                # setattr(form.instance, self.fk.get_attname(),
                # self.instance.pk)

                # We shouldn't really need to set this, but our model relies
                # on accessing the parent instance to determine the directory
                # name where files should be saved, and that might not be
                # possible if the old instance with no PK is still at the
                # other end of the FK, so replace it now.
                setattr(form.instance, self.fk.name, self.instance)

        return super(BetterInlineFormSet, self).save(commit)

    def try_save_if_not_valid(self, form, instance):
        if self.ignore_errors and not form.is_valid():
            instance.save()
            return True
        return False

    def save_new(self, form, commit=True):
        """Saves and returns a new model instance for the given form."""
        if self.try_save_if_not_valid(form, form.instance):
            return form.instance

        # Copied from BaseInlineFormSet.save_new() to assign the model to the
        # related field, instead of the model's PK to the FK field, so that the
        # model can access its related model while saving.

        # Use commit=False so we can assign the parent key afterwards, then
        # save the object.
        obj = form.save(commit=False)
        setattr(obj, self.fk.name, self.instance)
        if commit:
            obj.save()
        # form.save_m2m() can be called via the formset later on if commit=False
        if commit and hasattr(form, 'save_m2m'):
            form.save_m2m()
        return obj

    def save_existing(self, form, instance, commit=True):
        """Saves and returns an existing model instance for the given form."""
        if self.try_save_if_not_valid(form, instance):
            return instance
        else:
            return super(BetterInlineFormSet, self).save_existing(form, instance, commit)

    @property
    def management_form(self):
        """Returns the ManagementForm instance for this FormSet."""
        if self.is_bound:
            form = ManagementForm(self.data, auto_id=self.auto_id, prefix=self.prefix)
            if not form.is_valid():
                raise ValidationError("ManagementForm data is missing or has "
                        "been tampered with: %s" % form.errors)
        return super(BetterInlineFormSet, self).management_form


class InlineFormSetWithClientSideDeletion(BetterInlineFormSet):
    """
    Removing an item from the formset in Javascript should delete the
    corresponding model from the database, as though the form was submitted
    with the DELETE parameter.
    """

    def __init__(self, *args, **kwargs):
        if 'extra' not in kwargs:
            # Sensible default for "extra" on this kind of form: if we have no
            # forms already, then we need to add one "extra" for the JavaScript
            # to use as a template. Otherwise we have at least one, and the JS
            # will use that, so we don't need or want an extra.

            if 'instance' in kwargs:
                # doesn't exist yet, so we can't call its queryset() method
                queryset = self.model._default_manager.filter(
                    **{self.fk.name: kwargs['instance']})
                kwargs['extra'] = 0 if queryset.exists() else 1
            else:
                kwargs['extra'] = 1

        super(InlineFormSetWithClientSideDeletion, self).__init__(*args,
            **kwargs)

    def save_existing_objects(self, commit=True, ignore_errors=False):
        updated = super(InlineFormSetWithClientSideDeletion, self).save_existing_objects(commit)

        pks_mentioned_in_forms = []
        for form in self.initial_forms:
            pk_name = self._pk_field.name
            raw_pk_value = form._raw_value(pk_name)

            # clean() for different types of PK fields can sometimes return
            # the model instance, and sometimes the PK. Handle either.
            pk_value = form.fields[pk_name].clean(raw_pk_value)
            pk_value = getattr(pk_value, 'pk', pk_value)
            pks_mentioned_in_forms.append(pk_value)

        # Remove objects that are in the queryset but not listed on the form.
        # TODO FIXME should we only do this if commit=True? BaseInlineFormSet
        # always deletes in Django 1.5, but that looks wrong to me, because it
        # only saves updated objects if commit=True.
        to_delete = self.get_queryset().exclude(pk__in=pks_mentioned_in_forms)
        to_delete.delete()
        self.deleted_objects.extend(to_delete)

        return updated
