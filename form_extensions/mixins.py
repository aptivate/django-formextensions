from __future__ import absolute_import, unicode_literals

from django.contrib import messages
from django.core.exceptions import ValidationError
from django.db.models.fields import FieldDoesNotExist

from django.forms import FileField
from django.forms.formsets import DELETION_FIELD_NAME, ORDERING_FIELD_NAME
from django.forms.util import ErrorDict
from django.forms.widgets import Textarea, Select
from django.http.response import HttpResponseRedirect
from django.utils.translation import ugettext as _

from crispy_forms.layout import Field, Layout, LayoutObject, Submit
from crispy_forms.helper import FormHelper

from .widgets import ReadOnlyWidget, ReadOnlyFileWidget


class SaveValidationMixin(object):
    # Say WHY validation failed

    def save(self, commit=True):
        """
        Saves this ``form``'s cleaned_data into model instance
        ``self.instance``.

        If commit=True, then the changes to ``instance`` will be saved to the
        database. Returns ``instance``.
        """
        if self.instance.pk is None:
            fail_message = 'created'
        else:
            fail_message = 'changed'
        opts = self.instance._meta

        if self.errors:
            raise ValueError(
                "The %s could not be %s because the data didn't validate: %s"
                % (opts.object_name, fail_message, dict(self.errors))
            )

        return super(SaveValidationMixin, self).save(commit)


class PureFormMixin(object):
    required_fields = ()
    field_labels = {}

    def __init__(self, *args, **kwargs):
        super(PureFormMixin, self).__init__(*args, **kwargs)

        # Basic automatic layout, can be overridden by subclasses
        fields = []

        for field_name in self.fields:
            field = Field(field_name, wrapper_class="pure-control-group")
            fields.append(field)

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.disable_csrf = True
        self.helper.render_hidden_fields = True
        self.helper.layout = Layout(*fields)

        for field_name, label in self.field_labels.iteritems():
            self.fields[field_name].label = label

        self.set_required_fields()

        for bound_field in self:
            self.tweak_widgets(bound_field)

        # Errors are set before the changes to the fields are completed.
        # To fix this, we clear the errors after all the changes have been made
        self._errors = None

    def set_required_fields(self):
        for field_name in self.required_fields:
            bound_field = self[field_name]
            bound_field.field.required = True


    def _get_field(self, bound_field):
        name = bound_field.name
        model = bound_field.form._meta.model
        meta = model._meta
        field = None

        try:
            from hvad.exceptions import WrongManager
            try:
                field = meta.get_field(name)
            except (FieldDoesNotExist, WrongManager) as e:
                try:
                    field = meta.translations_model._meta.get_field(name)
                except FieldDoesNotExist:
                    raise e
            else:
                raise e
        except ImportError:
            field = meta.get_field(name)

        return field


    def tweak_widgets(self, bound_field):
        # copy the updated required value into widgets
        bound_field.field.widget.is_required = bound_field.field.required

        if bound_field.field.required:
            """
            HTML5 validation is tricky on file fields, because they're not
            populated if a file has already been uploaded. So we should not
            set the "required" HTML attribute on them in that case.
            """

            if (
                isinstance(bound_field.field, FileField) and
                bound_field.value()
            ):
                pass
            else:
                bound_field.field.widget.attrs['required'] = "required"

        # workaround for django bug #22206, set maxlength on textareas
        if isinstance(bound_field.field.widget, Textarea):
            model_field = self._get_field(bound_field)
            if model_field.max_length is not None:
                bound_field.field.widget.attrs['maxlength'] = \
                    model_field.max_length

        if isinstance(bound_field.field.widget, ReadOnlyWidget):
            if (
                hasattr(bound_field.field, 'choices') and
                bound_field.field.choices
            ):
                field_value_name = "get_{0}_display".format(
                    bound_field.name
                )
                value = getattr(self.instance, field_value_name)()
            else:
                value = bound_field.value()

            self.update_read_only_field_template(bound_field, value)

    def update_read_only_field_template(self, bound_field, value):
        prefix = self.prefix or ""
        field_id = prefix + "-" + bound_field.name

        template = (
            "<span>{0}</span><input type=\"hidden\" "
            "id=\"{1}\" name=\"{2}\" value=\"{3}\" />".format(
                value,
                "id_" + field_id,
                field_id,
                bound_field.value(),
            )
        )

        bound_field.field.widget.attrs['template'] = template

    # This will be set to True if the app wants us to validate the form
    # even if it's unbound.
    validate_unbound = False

    # Support validating of unbound forms, to display errors on GET with
    # ?type=submit
    def full_clean(self, validate_unbound=False):
        """
        Cleans all of self.data and populates self._errors and
        self.cleaned_data.
        """
        self._errors = ErrorDict()
        self.validate_unbound = validate_unbound

        if not self.is_bound and not validate_unbound:
            # default behaviour to match normal Django forms
            return

        self.cleaned_data = {}
        # If the form is permitted to be empty, and none of the form data has
        # changed from the initial data, short circuit any validation.
        self._clean_fields()
        self._clean_form()
        self._post_clean()

    def _clean_fields(self):
        for name, field in self.fields.items():
            # value_from_datadict() gets the data from the data dictionaries.
            # Each widget type knows how to retrieve its own data, because some
            # widgets split data over several HTML fields.
            value = self[name].value()

            if name in (ORDERING_FIELD_NAME, DELETION_FIELD_NAME):
                # continue  # don't try to validate this field
                # TODO: Not clear why these are being skipped. The django
                # formsets code does access ORDERING_FIELD_NAME and
                # DELETION_FIELD_NAME from a forms cleaned_data!
                pass

            try:
                try:
                    if isinstance(field, FileField):
                        initial = self.initial.get(name, field.initial)
                        value = field.clean(value, initial)
                    else:
                        value = field.clean(value)
                except ValidationError as e:
                    raise
                except Exception as e:
                    raise ValidationError(e)
                self.cleaned_data[name] = value
                if hasattr(self, 'clean_%s' % name):
                    value = getattr(self, 'clean_%s' % name)()
                    self.cleaned_data[name] = value
            except ValidationError as e:
                self._errors[name] = self.error_class(e.messages)
                if name in self.cleaned_data:
                    del self.cleaned_data[name]

from django.forms.widgets import NullBooleanSelect


class DisplayOnlyFormMixin(object):
    def __init__(self, *args, **kwargs):
        super(DisplayOnlyFormMixin, self).__init__(*args, **kwargs)
        for name in self.fields:
            choices = getattr(self.fields[name], 'choices', ())
            if isinstance(self.fields[name], FileField):
                self.fields[name].widget = ReadOnlyFileWidget(choices=choices)
            else:
                self.fields[name].widget = ReadOnlyWidget(choices=choices)
            # If we don't do this then BetterModelForm will stick required
            # classes on labels even though we're just displaying.
            self.fields[name].required = False


class ReadOnlyFormMixin(PureFormMixin):
    excluded_fields = ()

    def __init__(self, *args, **kwargs):
        super(ReadOnlyFormMixin, self).__init__(*args, **kwargs)
        self.tweak_layout_recursive(self.helper.layout)

    def construct_formset(self, *args, **kwargs):
        """
        Tweak all widgets on all formsets to make them read-only too.
        """

        formset = super(ReadOnlyFormMixin, self).construct_formset(*args, **kwargs)

        formset.can_delete = False
        formset.max_num = len(formset.forms)
        for subform in formset:
            for bound_field in subform:
                self.tweak_widgets(bound_field)

        return formset

    def tweak_widgets(self, bound_field):
        if bound_field.name not in self.excluded_fields:
            widget = bound_field.field.widget
            if isinstance(widget, Select):
                widget.attrs['disabled'] = 'disabled'
            else:
                widget.attrs['readonly'] = 'readonly'
        return super(ReadOnlyFormMixin, self).tweak_widgets(bound_field)

    def tweak_layout_recursive(self, layout_item):
        """
        Change all Submit buttons to hidden input, so javascript still works
        but users can't click on them.
        """

        if isinstance(layout_item, Submit):
            layout_item.input_type = 'hidden'

        if isinstance(layout_item, LayoutObject):
            for child in layout_item:
                self.tweak_layout_recursive(child)


class RequiredNullBooleanMixin(object):
    '''
    Null Boolean Field gives Unknown/Yes/No, but unknown counts as a value so
    required test passes even when Unknown is selected as option.

    clean_nullboolean_field is a decorator for creating clean_<fieldname>
    methods that can validate NullBooleanFields which allow null on save,
    but don't allow it on submit.

    This approach was taken because:
    - field's validator are not run when value is None (null) and hence
      can't be used for this purpose
    - clean() method can validate, but cannot associate validation errors
      with particular field which is an UX failure
    - decorator has to be part of a form instance to have access to
      instance's cleaned_data since method is not provided a value
    - same reason requires a separate method for each such field

    I am not able to do this at this time in cleaner, but still *generic* way
    '''
    def clean_nullboolean_field(self, name):
        def clean_fieldname():
            value = self.cleaned_data[name]
            if value is not True and value is not False:
                raise ValidationError(u"This field requires Yes or No value.")
            return value
        return clean_fieldname

    def __init__(self, *args, **kwargs):
        super(RequiredNullBooleanMixin, self).__init__(*args, **kwargs)
        required_fields = self.instance.get_required_for_submission()
        for name in self.fields:
            if name in required_fields:
                if isinstance(self.fields[name].widget, NullBooleanSelect):
                    setattr(self, 'clean_' + name,
                            self.clean_nullboolean_field(name))


class SoftRequiredMixin(object):
    def __init__(self, *args, **kwargs):
        super(SoftRequiredMixin, self).__init__(*args, **kwargs)
        required_fields = self.instance.get_required_for_submission()
        for name in self.fields:
            if name in required_fields:
                self.fields[name].required = True


class SaveWithoutValidationViewMixin(object):
    def form_valid(self, form):
        # make form errors testable by saving the context in the redirect response
        redirect = super(SaveWithoutValidationViewMixin, self).form_valid(form)
        redirect.context_data = self.get_context_data(form=form)

        return redirect

    def save_data_without_validation(self, form):
        # Construct a new form with the required value turned off on all fields
        # that accept blank values, and use that to save the instance.
        all_optional_form = form.__class__(
            data=self.request.POST,
            files=self.request.FILES,
            instance=form.instance
        )

        for field in all_optional_form:
            # Skip form fields that don't represent a model field.
            if hasattr(form.instance, field.name):
                model_field = form.instance.__class__._meta.get_field(field.name)
                if model_field.blank:
                    field.field.required = False

        for name in all_optional_form.formsets.keys():
            formset = getattr(all_optional_form, name)

            for subform in formset:
                for field in subform:
                    field.field.required = False

        all_optional_form.save(ignore_errors=True)
        self.object = all_optional_form.instance

    def post(self, request, *args, **kwargs):
        if hasattr(self, 'object') and self.object:
            instance = self.object
        else:
            instance = None

        form = self.get_form(
            data=request.POST,
            files=request.FILES,
            instance=instance
        )

        if form.is_valid() and 'submit' in request.POST:
            return self.form_valid(form)
        return self.form_invalid(form)

    def get_form(self, data=None, files=None, **kwargs):
        # If we've been redirected here because of a form error, then we need
        # to validate the form again. We use form.full_clean(validate_unbound=True)
        # to force the form to validate itself even if unbound (our extension).
        # We must also add "form-submitted" to the CSS class, so that error
        # fields show up in red, etc.
        form = super(SaveWithoutValidationViewMixin, self).get_form(data,
            files, **kwargs)

        request_origin = self.request.GET.get('type')
        if request_origin == 'submit':
            # validate the form and subforms
            form.full_clean(validate_unbound=True)

        return form

    def form_invalid(self, form):
        self.save_data_without_validation(form)

        if not 'submit' in self.request.POST:
            messages.success(
                self.request,
                _("Your application has been saved. "
                  "You can now continue editing it.")
            )

        url = self.get_failure_url()
        url += ('?type=submit' if 'submit' in self.request.POST else '?type=save')
        redirect = HttpResponseRedirect(url)

        # make form errors testable by saving the context in the redirect response
        redirect.context_data = self.get_context_data(form=form)
        return redirect


class SaveFormWithErrorsMixin(object):
    def save(self, commit=True, ignore_errors=False):
        if self.is_valid():
            super(SaveFormWithErrorsMixin, self).save(commit)
        elif ignore_errors:
            # at least try to save the instance
            self.instance.save()
        else:
            # Not ignoring errors, so try to save anyway, this should throw
            # an exception.
            super(SaveFormWithErrorsMixin, self).save(commit)

        assert self.instance.pk is not None
        for formset_name in self.formsets.keys():
            formset = getattr(self, formset_name)
            formset.instance = self.instance
            formset.save(commit, ignore_errors)

        return self.instance


class CreateWithInlinesMixin(object):
    def __init__(self, data, files, *args, **kwargs):
        super(CreateWithInlinesMixin, self).__init__(data, files, *args, **kwargs)
        if 'initial' in kwargs:
            # Formsets treat this parameter completely differently: they
            # expect an array of values instead of just one. So remove it
            # because it will break formsets.
            kwargs.pop('initial')

        for name, formset_class in self.formsets.iteritems():
            # doesn't exist yet, so we can't call its queryset() method
            queryset = formset_class.model._default_manager.filter(
                **{formset_class.fk.name: self.instance})
            extra = 0 if queryset.exists() else 1

            formset = self.construct_formset(formset_class, data, files, *args,
                prefix=name, extra=extra, **kwargs)
            setattr(self, name, formset)

            helper = getattr(formset.form(), 'helper', None)
            if helper:
                formset.helper = helper

    def construct_formset(self, formset_class, data, files, prefix, extra,
            *args, **kwargs):
        return formset_class(data, files, *args, prefix=prefix, extra=extra,
            **kwargs)

    def is_valid(self):
        """
        Returns True if the form has no errors, nor any of the subforms
        (formsets), otherwise False.
        """

        if not super(CreateWithInlinesMixin, self).is_valid():
            return False

        for formset_name in self.formsets.keys():
            formset = getattr(self, formset_name)
            if not formset.is_valid():
                return False

        return True


    def full_clean(self, **kwargs):
        for formset_name in self.formsets.keys():
            # full_clean() is called during construction, when __init__ hasn't
            # yet constructed the formsets, so this attribute may not exist yet.
            formset = getattr(self, formset_name, [])
            for form in formset:
                form.full_clean(**kwargs)

        return super(CreateWithInlinesMixin, self).full_clean(**kwargs)
