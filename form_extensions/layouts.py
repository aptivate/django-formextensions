from crispy_forms.layout import LayoutObject, TEMPLATE_PACK
from django.template.loader import render_to_string


class Formset(LayoutObject):
    """
    Layout object. It renders an entire formset, as though it were a Field.

    Example::

        Formset("attached_files_formset")

    """
    template = "%s/formset.html" % TEMPLATE_PACK

    def __init__(self, formset_name_in_context, template=None, css_class=None):
        self.formset_name_in_context = formset_name_in_context
        self.css_class = css_class

        # crispy_forms/layout.py:302 requires us to have a fields property
        self.fields = []

        # Overrides class variable with an instance level variable
        if template:
            self.template = template

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK):
        formset = getattr(form, self.formset_name_in_context)

        context.push()
        try:
            context['wrapper'] = self
            context['formset'] = formset
            context['css_class'] = self.css_class
            return render_to_string(self.template, context)
        finally:
            context.pop()


class TableFormset(Formset):
    template = "%s/table_formset.html" % TEMPLATE_PACK


class Template(object):
    """
    Layout object. Renders a template.

    Examples::

        Template("bootstrap/foobar.html")
    """

    def __init__(self, template):
        self.template = template

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK):
        return render_to_string(self.template, context)


class Text(object):
    """
    Layout object. Renders text using a template. The text is available in the
    context of the template using "{{ text }}".

    Examples::

        Text("Hello!") => <p class="crispy-text">Hello!</p>
        Text("Hello!", template="mytext.html") =>
            <span class="mytext">Hello!</span>
    """

    template = "%s/text.html" % TEMPLATE_PACK

    def __init__(self, text, template=None, extra_context=None):
        self.text = text
        if template is not None:
            self.template = template
        self.context = extra_context or {}

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK):
        context.update(self.context)
        context.push()
        try:
            context['text'] = self.text
            return render_to_string(self.template, context)
        finally:
            context.pop()
