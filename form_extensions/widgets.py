# -*- encoding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals

from datetime import date
from os.path import basename

from django.forms.widgets import ClearableFileInput, Textarea, Widget, \
    CheckboxInput, MultiWidget, Select
from django.template import Template, Context
from django.utils.html import conditional_escape, format_html
from django.utils.encoding import force_text

import floppyforms as forms
from form_utils.widgets import ImageWidget


class DateInput2(forms.TextInput):
    # Javascript & CSS available through assets:
    # widget_js_date_input_2 or widget_js_all
    # widget_css_date_input_2 or widget_css_all
    def get_context(self, name, value, attrs):
        ctx = super(DateInput2, self).get_context(name, value, attrs)
        ctx['attrs']['placeholder'] = 'YYYY-MM-DD'
        ctx['attrs']['class'] = 'date-picker'
        return ctx


class YearMonthDateSelectorWidget(MultiWidget):
    def __init__(self, attrs=None):
        if not attrs:
            attrs = {}
        attrs.update({'class': 'year-month-date-selector'})
        months = [(1, 'January'),
                  (2, 'February'),
                  (3, 'March'),
                  (4, 'April'),
                  (5, 'May'),
                  (6, 'June'),
                  (7, 'July'),
                  (8, 'August'),
                  (9, 'September'),
                  (10, 'October'),
                  (11, 'November'),
                  (12, 'December')]
        this_year = (date.today()).year
        years = [(year, year) for year in range(2010, this_year + 7)]
        months.insert(0, ('', '----'))
        years.insert(0, ('', '----'))
        widgets = (Select(attrs=attrs, choices=months),
                   Select(attrs=attrs, choices=years))
        super(YearMonthDateSelectorWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        value_list = [None, None]
        if value:
            if isinstance(value, str) or isinstance(value, unicode):
                # e.g. 2010-10-01; [::-1] reverses list
                value_list = [int(v) for v in value.split("-")[:2][::-1]]
            else:  # Date
                value_list = [value.month, value.year]
        return value_list

    def format_output(self, rendered_widgets):
        return u''.join(rendered_widgets)

    def value_from_datadict(self, data, files, name):
        month = self.widgets[0].value_from_datadict(data, files, name + '_0')
        year = self.widgets[1].value_from_datadict(data, files, name + '_1')
        try:
            value_to_store = date(day=15, month=int(month), year=int(year))
        except ValueError:
            return ''
        else:
            return str(value_to_store)


class YearDateSelectorWidget(forms.widgets.NumberInput):
    def __init__(self, attrs=None):
        wattrs = {
            'placeholder': 'YYYY',
            'size': 4,
            'maxlength': 4,
            'class': 'year-date-selector'
        }
        if attrs:
            wattrs.update(attrs)
        super(YearDateSelectorWidget, self).__init__(wattrs)

    def render(self, name, value, attrs=None):
        if type(value) in (str, unicode):
            value = value
        elif type(value) == date:
            value = value.year
        else:
            value = ""
        return super(YearDateSelectorWidget, self).render(
            name, value, attrs)

    def value_from_datadict(self, data, files, name):
        value_to_store = None
        year = data.get(name, "")  # Cause value error if we don't have it
        try:
            year = int(year)
        except ValueError:
            return super(YearDateSelectorWidget, self).value_from_datadict(
                data, files, name)
        if year:
            value_to_store = date(day=15, month=7, year=year)
        return value_to_store


class BetterSelectMultiple(forms.SelectMultiple):
    def __init__(self, attrs=None, choices=()):
        widget_attrs = {
            'size': 10,
            'class': 'widget-multi-select'
        }
        if attrs:
            widget_attrs.update(attrs)
        super(BetterSelectMultiple, self).__init__(widget_attrs, choices)


class BetterFileInput(ClearableFileInput):
    # Javascript available through assets:
    # widget_js_better_file_input or widget_js_all

    template = u"""
        <div class="field-clear">
            {{ initial_value_wrapper }}

            {% if not widget.attrs.readonly %}
                {% if url and not widget.is_required %}
                    {{ clear_template_1 }}
                    {{ file_input_wrapper }}
                    {{ clear_template_2 }}
                {% else %}
                    {{ file_input_wrapper }}
                {% endif %}
            {% endif %}
        </div>"""
    initial_value_wrapper = u"""
        {% if url %}
            <span class="field-clear-initial"><a href="{{ url }}">{{ filename }}</a></span>
        {% else %}
            <span class="field-clear-initial-empty"><a href="{{ url }}">{{ filename }}</a></span>
        {% endif %}
        """
    clear_template_1 = u"""
        <script>
            document.write("" +
                '<span class="field-clear-check-button pure-button pure-button-xsmall pure-button-error">Remove</span>' +
                '<span class="field-clear-cancel pure-button-xsmall pure-button pure-button-hidden">Cancel</span>' +
                '<div class="field-clear-hidden-input pure-button-hidden">{{ clear }}</div>');
        </script>
        """
    file_input_wrapper = """
        <div class="browse-box">
        {{ input }}
        {{ html5_validation_container }}
        </div>
        """
    html5_validation_container = u"""
        <p class="field-error-inline-html5validation"></p>
        """
    clear_template_2 = u"""
        <noscript>
            <label for="{{ clear_checkbox_id }}" class="field-clear-check">
                Remove file: {{ clear_checkbox }} (tick and save)
            </label>
        </noscript>
        """
    vars_to_render = ('initial_value_wrapper', 'clear_template_1', 'clear_template_2',
                      'html5_validation_container', 'file_input_wrapper', 'template')

    def get_context_data(self, name, value, attrs=None):
        context = {
            'widget': self,
            'input': super(ClearableFileInput, self).render(name, value, attrs),
            'required': self.is_required,
        }

        if value and hasattr(value, "url"):
            context['url'] = value.url
            context['filename'] = basename(force_text(value))

            if not self.is_required:
                checkbox_name = self.clear_checkbox_name(name)
                checkbox_id = self.clear_checkbox_id(checkbox_name)
                context['clear_checkbox_name'] = conditional_escape(checkbox_name)
                context['clear_checkbox_id'] = conditional_escape(checkbox_id)
                context['clear'] = CheckboxInput().render(checkbox_name, False, attrs={'id': checkbox_id})

        for attribute_name in self.vars_to_render:
            context[attribute_name] = getattr(self, attribute_name)

        return context

    def render(self, name, value, attrs=None):
        '''Same render method as ClearableFileInput has except that it wraps
        displayed file name with os.path.basename for nicer output.'''

        context = Context(self.get_context_data(name, value, attrs))

        for name in self.vars_to_render:
            context[name] = Template(context[name]).render(context)

        return context['template']

    def value_from_datadict(self, data, files, name):
        '''
        Practically same as ClearableFileInput except that when user
        contradicts themselves, we return upload (upload wins).
        '''
        upload = super(ClearableFileInput, self).value_from_datadict(data, files, name)
        if not self.is_required and CheckboxInput().value_from_datadict(
                data, files, self.clear_checkbox_name(name)) and not upload:
            # False signals to clear any existing value, as opposed to just None
            return False
        return upload


class BetterImageInput(ClearableFileInput, ImageWidget):
    template = u"""
            <span class="field-clear-initial">%(image)s</span>
            <br />
            <script>document.write('<span class="field-clear-check-button pure-button pure-button-xsmall pure-button-error">Remove</span><span class="field-clear-cancel pure-button-xsmall pure-button pure-button-hidden">Cancel</span>')</script>
            <div class="browse-box">%(input)s</div>
    """
    template_with_clear = u"""
        <label for="%(clear_checkbox_id)s" class="field-clear-check">
            Remove file: %(clear)s (tick and save)
        </label>"""
    template_with_initial = u"""
        <span class="field-clear">
            %(input)s
            <noscript>%(clear_template)s</noscript>
            <script>document.write('<span class="field-clear-hidden-input pure-button pure-button-xsmall pure-button-hidden">%(clear)s</span>')</script>
        </span>"""


class TextareaWordLimit(Textarea):
    # Javascript & CSS available through assets:
    # widget_js_all

    def __init__(self, attrs=None):
        w_attrs = {'class': "input-word-limit"}
        if attrs:
            w_attrs.update(attrs)
        super(TextareaWordLimit, self).__init__(w_attrs)


class PrintValueWidget(Widget):
    '''
    Prints value

    You can control the output by providing following attributes:
    - formatter, which should be a callable that receives a value and returns
    the formatted version of it (default: returns unchanged or verbose value
      for fields with choices)
    - template, which defines HTML template that will be used (default:
        <span>{0}</span>)
    '''
    template = u"<span>{0}</span>"

    def __init__(self, attrs=None, choices=()):
        super(PrintValueWidget, self).__init__(attrs)
        if choices:
            self.choices = choices

    @staticmethod
    def find_choice(value, choices=()):
        for k, v in choices:
            if isinstance(v, (list, tuple)):
                # This is an optgroup, so look inside the group for options
                for k2, v2 in v:
                    if value == k2:
                        return v2
            else:
                if value == k:
                    return v
        if value is None:
            value = ""
        return value

    def formatter(self, value):
        if getattr(self, 'choices', None) and value:
            return self.find_choice(value, self.choices)
        if value is None:
            value = ""
        return value

    def render(self, name, value, attrs=None):
        formatter = self.attrs.get('formatter', self.formatter)
        template = self.attrs.get('template', self.template)
        return format_html(template.format(formatter(value)))


class ReadOnlyWidget(PrintValueWidget):
    # By default PrintValueWidget behaves like read only output
    def __init__(self, attrs=None, choices=()):
        super(ReadOnlyWidget, self).__init__(attrs, choices)


class ReadOnlyFileWidget(PrintValueWidget):
    template = u'<a href="{0}">{1}</a>'

    def __init__(self, attrs=None, choices=()):
        super(ReadOnlyFileWidget, self).__init__(attrs, choices)

    def formatter(self, file):
        anchor = file.name.rsplit("/", 1)[-1]
        return (file.url, anchor)

    def render(self, name, value, attrs=None):
        if not value:
            return u""
        return format_html(self.template.format(*self.formatter(value)))
