# -*- encoding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals

from django.forms.fields import CharField, TypedChoiceField
from django.forms.models import ModelChoiceField
from django.forms.widgets import RadioSelect

from crispy_forms.layout import TEMPLATE_PACK, Field, MultiField

from .widgets import TextareaWordLimit


class CharFieldWordLimit(CharField):
    widget = TextareaWordLimit

    def __init__(self, max_length=None, min_length=None,
                 word_limit=0, *args, **kwargs):
        self.word_limit = word_limit
        super(CharFieldWordLimit, self).__init__(
            max_length, min_length, *args, **kwargs)

    def widget_attrs(self, widget):
        attrs = super(CharFieldWordLimit, self).widget_attrs(widget)
        # Put it as data on textarea so Javascript can pick it up
        attrs.update({'data-word-limit': int(self.word_limit)})
        return attrs


class DebugField(Field):
    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK):
        import pdb; pdb.set_trace()
        return super(DebugField, self).render(
            form,
            form_style,
            context,
            template_pack
        )


class PureFieldset(MultiField):
    template = "pure/fieldset_wrapper.html"
    field_template = "pure/fieldset_field.html"
    css_class = "pure-form-fieldset-group"

    def __init__(self, label, *fields, **kwargs):
        kwargs.setdefault('css_class', self.css_class)

        super(PureFieldset, self).__init__(label, *fields, **kwargs)

        for field in fields:
            if (
                not hasattr(field, 'wrapper_class') or
                field.wrapper_class is None
            ):
                field.wrapper_class = "pure-control-group"


class ModelChoiceByAttributeField(ModelChoiceField):
    def __init__(self, queryset, attribute_name, *args, **kwargs):
        self.attribute_name = attribute_name
        super(ModelChoiceByAttributeField, self).__init__(queryset, *args, **kwargs)

    def label_from_instance(self, obj):
        return getattr(obj, self.attribute_name)


class YesNoField(TypedChoiceField):
    def __init__(self, *args, **kwargs):
        super(YesNoField, self).__init__(*args, **kwargs)
        self.widget = RadioSelect()
        self.choices = (
            (False, "No"),
            (True, "Yes"),
        )

        self.coerce = self._coerce

    def _coerce(self, value):
        if value in (True, 'True', '1'):
            return True
        elif value in (False, 'False', '0'):
            return False
        else:
            return None

    def to_python(self, value):
        return self._coerce(value)
