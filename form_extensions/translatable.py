# coding: utf-8
from __future__ import (absolute_import,
                        division, print_function, unicode_literals)

from django.utils.translation import get_language
from form_utils.forms import BetterModelForm, BetterModelFormMetaclass
from hvad.forms import TranslatableModelForm, TranslatableModelFormMetaclass

from six import with_metaclass


class TranslatableBetterModelFormMetaClass(
        TranslatableModelFormMetaclass, BetterModelFormMetaclass):
    def __init__(cls, name, bases, attr):
        TranslatableModelFormMetaclass.__init__(cls, name, bases, attr)
        BetterModelFormMetaclass.__init__(cls, name, bases, attr)

    def __new__(cls, name, bases, attrs):
        from form_utils.forms import (
            get_fieldsets, get_fields_from_fieldsets, _get_meta_attr,
            _set_meta_attr, get_row_attrs
        )
        attrs['base_fieldsets'] = get_fieldsets(bases, attrs)
        fields = get_fields_from_fieldsets(attrs['base_fieldsets'])
        if (_get_meta_attr(attrs, 'fields', None) is None and
                _get_meta_attr(attrs, 'exclude', None) is None):
            _set_meta_attr(attrs, 'fields', fields)
        attrs['base_row_attrs'] = get_row_attrs(bases, attrs)

        return TranslatableModelFormMetaclass.__new__(cls, name, bases, attrs)


class TranslatableBetterModelForm(with_metaclass(
        TranslatableBetterModelFormMetaClass,
        BetterModelForm, TranslatableModelForm)):

    def __init__(self, *args, **kwargs):
        self.language = kwargs.pop('language', get_language())
        super(TranslatableBetterModelForm, self).__init__(*args, **kwargs)
