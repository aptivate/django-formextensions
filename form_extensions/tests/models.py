from django.db import models
from django.db.models.base import Model


class MockReport(Model):
    not_required = models.TextField(blank=True)
    required = models.TextField(blank=True)
    accepted = models.DateTimeField(null=True, blank=True)

    class Meta:
        app_label = 'form_extensions'

    def get_required_for_submission(self):
        return ('required',)


class MockInlineReport(Model):
    report = models.ForeignKey(MockReport)
    inline_required = models.TextField(blank=True)
    inline_not_required = models.TextField(blank=True)

    class Meta:
        app_label = 'form_extensions'

    def get_required_for_submission(self):
        return ('inline_required',)


class MockReportWithNullBoolean(Model):
    bool_not_required = models.NullBooleanField(null=True, blank=True)
    bool_required = models.NullBooleanField(null=True, blank=True)
    text_not_required = models.TextField(blank=True)
    text_required = models.TextField(blank=True)

    class Meta:
        app_label = 'form_extensions'

    def get_required_for_submission(self):
        return ('bool_required', 'text_required')


class MockCall(Model):
    call_id = models.CharField(max_length=30, verbose_name='Call ID')
    title = models.CharField(max_length=256)

    class Meta:
        app_label = 'form_extensions'
