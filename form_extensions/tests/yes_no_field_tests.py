# -*- encoding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals

from django.db import models
from django.forms import ModelForm
from django.forms.forms import BoundField

from ..fields import YesNoField


class TestYesNoFieldModel(models.Model):
    yes_no_field = models.NullBooleanField()


class TestYesNoFieldForm(ModelForm):
    yes_no_field = YesNoField()

    class Meta:
        model = TestYesNoFieldModel
        fields = ['yes_no_field', ]


def test_field_is_rendered_as_radio_select():
    model = TestYesNoFieldModel()
    model.yes_no_field = True

    form = TestYesNoFieldForm(instance=model)

    field = form.fields['yes_no_field']
    bound_field = BoundField(form, field, 'yes_no_field')
    html = bound_field.as_widget()

    expected_html = ('<ul id="id_yes_no_field">'
        '<li><label for="id_yes_no_field_0">'
            '<input id="id_yes_no_field_0" name="yes_no_field" type="radio" '
                'value="False" /> No</label></li>\n'
        '<li><label for="id_yes_no_field_1">'
            '<input checked="checked" id="id_yes_no_field_1" '
                'name="yes_no_field" type="radio" '
                'value="True" /> Yes</label></li></ul>')

    assert expected_html == html


def test_true_value_is_coerced_to_boolean_true():
    form = TestYesNoFieldForm(dict(yes_no_field='True'))
    form.is_valid()
    assert form.cleaned_data['yes_no_field'] is True


def test_false_form_value_is_coerced_to_boolean_false():
    form = TestYesNoFieldForm(dict(yes_no_field='False'))
    form.is_valid()
    assert not form.cleaned_data['yes_no_field']


def test_false_clean_value_is_coerced_to_boolean_false():
    field = YesNoField()
    assert not field.clean('False')


def test_to_python_method_returns_false_for_false():
    assert not YesNoField().to_python('False')


def test_field_coerce_function_returns_false_for_false():
    assert not YesNoField().coerce('False')
