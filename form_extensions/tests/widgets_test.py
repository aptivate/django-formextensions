from ..widgets import PrintValueWidget, ReadOnlyFileWidget


#
# PrintValueWidget
#
def test_find_choice_returns_empty_string_when_no_value():
    choices = (
        ('a', 'A'),
        ('b', 'B'),
    )
    assert PrintValueWidget.find_choice(None, choices) == ""


def test_find_choice_returns_original_value_when_no_match():
    choices = (
        ('a', 'A'),
        ('b', 'B'),
    )
    assert PrintValueWidget.find_choice('c', choices) == "c"


def test_find_choice_returns_correct_label_in_flat_choices():
    choices = (
        ('a', 'A'),
        ('b', 'B'),
    )
    assert PrintValueWidget.find_choice('a', choices) == 'A'


def test_find_choice_return_correct_label_in_nested_choices():
    choices = (
        ('a', 'A'),
        ('b', 'B'),
        ('d', (
            ('c', 'C'),
            ('e', 'E'),
        )
        )
    )
    assert PrintValueWidget.find_choice('c', choices) == 'C'


def test_formatter_returns_empty_string_when_value_is_none():
    widget = PrintValueWidget()
    assert widget.formatter(None) == ''


def test_formatter_returns_value_when_no_choices():
    widget = PrintValueWidget()
    assert widget.formatter('abc') == 'abc'


def test_formatter_returns_correct_choice_label_when_given_choices():
    widget = PrintValueWidget()
    widget.choices = (
        ('a', 'A'),
        ('b', 'B'),
    )
    assert widget.formatter('b') == 'B'


#
# ReadOnlyFileWidget
#
class F(object):
    def __init__(self, url, name):
        self.url = url
        self.name = name


def test_formatter_returns_only_file_name_as_anchor_text():
    path = '/some/very/long/path/filename.txt'
    f = F(path, path)
    widget = ReadOnlyFileWidget()

    url, anchor = widget.formatter(f)
    assert anchor == 'filename.txt'
    assert url == f.url

    path = 'filename.txt'
    f = F(path, path)
    widget = ReadOnlyFileWidget()

    url, anchor = widget.formatter(f)
    assert anchor == 'filename.txt'
    assert url == f.url


def test_render_returns_empty_string_when_no_value():
    widget = ReadOnlyFileWidget()
    assert widget.render('name', None) == ""


def test_render_returns_html_link_element_when_there_is_value():
    expected = '<a href="/some/very/long/path/filename.txt">filename.txt</a>'
    widget = ReadOnlyFileWidget()
    path = '/some/very/long/path/filename.txt'
    f = F(path, path)
    assert widget.render('name', f) == expected
