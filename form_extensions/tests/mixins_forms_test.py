import os
import pytest

from django.core.exceptions import ValidationError
from django.forms import ModelForm
from django.forms.models import inlineformset_factory


from .models import MockReport, MockReportWithNullBoolean, MockCall, MockInlineReport

from ..formsets import BetterInlineFormSet
from ..mixins import (
    SoftRequiredMixin,
    DisplayOnlyFormMixin,
    RequiredNullBooleanMixin,
    ReadOnlyFormMixin,
    CreateWithInlinesMixin
)
from ..widgets import ReadOnlyWidget


#############################################################
# SoftRequiredMixin Tests
#############################################################
@pytest.fixture
def soft_required_form():
    # Note:
    # - The SoftRequiredMixin must come first.
    # - The model must have the get_required_for_submission method
    class SoftRequiredForm(SoftRequiredMixin, ModelForm):
        class Meta:
            model = MockReport
            exclude = ()
    return SoftRequiredForm()


def test_soft_required_mixin_adds_required_to_required_fields(soft_required_form):  # nopep8
    assert soft_required_form.fields['required'].required is True


def test_soft_required_mixin_does_not_add_required_to_non_required_fields(soft_required_form):  # nopep8
    assert soft_required_form.fields['not_required'].required is False


#############################################################
# DisplayOnlyFormMixin Tests
#############################################################
@pytest.fixture
def display_only_form():
    class DisplayOnlyForm(DisplayOnlyFormMixin, ModelForm):
        class Meta:
            model = MockReport
            exclude = ()
    return DisplayOnlyForm()


def test_display_only_form_mixin_sets_all_field_widgets_to_read_only(display_only_form):  # nopep8
    for field in display_only_form.fields.values():
        assert isinstance(field.widget, ReadOnlyWidget)


def test_display_only_form_mixin_sets_all_field_required_to_false(display_only_form):  # nopep8
    for field in display_only_form.fields.values():
        assert field.required is False


#############################################################
# RequiredNullBooleanMixin Tests
#############################################################
@pytest.fixture
def required_null_boolean_form():
    class RequiredNullBooleanForm(RequiredNullBooleanMixin, ModelForm):
        class Meta:
            model = MockReportWithNullBoolean
            exclude = ()
    return RequiredNullBooleanForm()


def test_required_null_boolean_fields_sets_clean_attr_on_only_required_and_nullboolean_field(required_null_boolean_form):  # nopep8
    func_name = required_null_boolean_form.clean_bool_required.func_name
    assert func_name == 'clean_fieldname'
    # For all other fields in model, a clean method should not have been set
    with pytest.raises(AttributeError):
        required_null_boolean_form.clean_text_required.func_name
    with pytest.raises(AttributeError):
        required_null_boolean_form.clean_bool_not_required.func_name
    with pytest.raises(AttributeError):
        required_null_boolean_form.clean_text_not_required.func_name


def test_clean_nullboolean_raises_a_validation_error_for_unknown_value(required_null_boolean_form):  # nopep8
    required_null_boolean_form.cleaned_data = {}
    required_null_boolean_form.cleaned_data['bool_required'] = 'Unknown'
    with pytest.raises(ValidationError):
        required_null_boolean_form.clean_bool_required()


def test_clean_nullboolean_returns_value_if_true(required_null_boolean_form):
    required_null_boolean_form.cleaned_data = {}
    required_null_boolean_form.cleaned_data['bool_required'] = True
    assert required_null_boolean_form.clean_bool_required() is True


def test_clean_nullboolean_returns_value_if_false(required_null_boolean_form):
    required_null_boolean_form.cleaned_data = {}
    required_null_boolean_form.cleaned_data['bool_required'] = False
    assert required_null_boolean_form.clean_bool_required() is False


#############################################################
# ReadOnlyFormMixin Tests
#############################################################
@pytest.fixture
def readonly_form():
    class ReadOnlyForm(ReadOnlyFormMixin, ModelForm):
        excluded_fields = ('title',)

        class Meta:
            model = MockCall
            exclude = ()
    return ReadOnlyForm()


@pytest.fixture
def readonly_form_with_inline_formset():
    MockReportInline = inlineformset_factory(
        MockReport,
        MockInlineReport,
        formset=BetterInlineFormSet,
        exclude=(),
    )

    class ReadOnlyFormWithFormset(ReadOnlyFormMixin, CreateWithInlinesMixin, ModelForm):
        formsets = {
            'inline_reports': MockReportInline
        }

        class Meta:
            model = MockReport
            exclude = ()

    return ReadOnlyFormWithFormset(None, None)


def test_excluded_fields_on_read_only_form_not_readonly(readonly_form):
    assert 'readonly' not in readonly_form.fields['title'].widget.attrs


@pytest.mark.skipif(os.environ.get('JENKINS') == 'JENKINS', reason='cant create test models on jenkins')
@pytest.mark.django_db
def test_cant_delete_items_from_readonly_formsets(readonly_form_with_inline_formset):
    assert not readonly_form_with_inline_formset.inline_reports.can_delete


@pytest.mark.skipif(os.environ.get('JENKINS') == 'JENKINS', reason='cant create test models on jenkins')
@pytest.mark.django_db
def test_cant_add_items_to_readonly_formsets(readonly_form_with_inline_formset):
    inline_reports_formset = readonly_form_with_inline_formset.inline_reports

    maximum_number_of_forms = inline_reports_formset.max_num
    assert len(inline_reports_formset.forms) == maximum_number_of_forms
    assert inline_reports_formset.management_form.initial['MAX_NUM_FORMS'] == maximum_number_of_forms
