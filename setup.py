import os

os.environ['DJANGO_SETTINGS_MODULE'] = \
    'tests.settings'

from setuptools import setup
from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = ['-m', 'not apitest']
        self.test_suite = True

    def run_tests(self):
        # import here, cause outside the eggs aren't loaded
        import pytest
        pytest.main(self.test_args)


setup(
    name='django-formextensions',
    version='0.1',
    author='http://www.aptivate.org/',
    author_email='ilsss-team@aptivate.org',
    packages=['form_extensions'],
    include_package_data=True,
    url='https://github.com/aptivate/django-formextensions',
    license='LICENSE',
    description='Django form extensions',
    # long_description=open('README.md').read(),
    setup_requires = [ "setuptools_git >= 0.3", "pytest-runner" ],
    install_requires=[
        "Django >= 1.8, < 1.9",
        # "south >= 0.8.4",
        # "mock >= 1.0.1",
    ],
    tests_require=[
      'pytest',
      'pytest-django',
      'mock',
    ],
    # cmdclass={'test': PyTest},
    test_suite="tests",
)
